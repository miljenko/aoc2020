class Node():
    def __init__(self, value):
        self.value = value
        self.next = None

MAX_VAL = 1_000_000
cups = [int(n) for n in '952438716'] + list(range(10, MAX_VAL+1))
nodes_by_value = {}
prev_node = None
for n in cups:
    cur_node = Node(n)
    nodes_by_value[n] = cur_node
    if prev_node:
        prev_node.next = cur_node
    prev_node = cur_node
current_cup = nodes_by_value[cups[0]]
cur_node.next = current_cup # last -> first

for _ in range(10_000_000):
    picked_up = [current_cup.next, current_cup.next.next, current_cup.next.next.next]
    dest = current_cup.value - 1 if current_cup.value > 1 else MAX_VAL
    while dest in [p.value for p in picked_up]:
        dest -= 1
        if dest < 1:
            dest = MAX_VAL
    insert_after = nodes_by_value[dest]

    current_cup.next = picked_up[2].next
    picked_up[2].next = insert_after.next
    insert_after.next = picked_up[0]

    current_cup = current_cup.next

print(nodes_by_value[1].next.value * nodes_by_value[1].next.next.value)
