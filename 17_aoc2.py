from collections import defaultdict

ACTIVE, INACTIVE = '#', '.'
grid = defaultdict(lambda: INACTIVE)

with open('17_input.txt') as f:
    for y, line in enumerate(f):
        for x, c in enumerate(line.strip()):
            grid[x, y, 0, 0] = c

def neighbours(x, y, z, w):
    for i in (x-1, x, x+1):
        for j in (y-1, y, y+1):
            for k in (z-1, z, z+1):
                for l in (w-1, w, w+1):
                    if (i, j, k, l) != (x, y, z, w):
                        yield i, j, k, l

def get_next_state(coords, current_grid, current_state):
    active_neighbours = sum(current_grid[n] == ACTIVE for n in neighbours(*coords) if n in current_grid)
    if current_state == ACTIVE and (active_neighbours < 2 or active_neighbours > 3):
        return INACTIVE
    if current_state == INACTIVE and active_neighbours == 3:
        return ACTIVE
    return current_state

current_grid = grid.copy()
for _ in range(6):
    next_grid = current_grid.copy()
    for coords, state in current_grid.items():
        next_grid[coords] = get_next_state(coords, current_grid, state)
        # process neighbours too to grow the grid
        for neighbour_coords in neighbours(*coords):
            if neighbour_coords not in next_grid:
                next_grid[neighbour_coords] = get_next_state(neighbour_coords, current_grid, INACTIVE)

    current_grid = next_grid.copy()

print(sum(v == ACTIVE for v in current_grid.values()))
