moves = []
with open('12_input.txt') as f:
    for line in f:
        moves.append((line[0], int(line[1:])))

DIRECTIONS = 'NESW' # clockwise order

def step(action, val):
    if action == 'N': return 0, val
    if action == 'E': return val, 0
    if action == 'S': return 0, -val
    if action == 'W': return -val, 0

def move(x, y, facing, action, val):
    dx, dy = 0, 0
    if action in DIRECTIONS:
        dx, dy = step(action, val)
    elif action == 'F':
        dx, dy = step(facing, val)
    elif action in 'RL':
        deg = val if action == 'R' else -val
        cur_idx = DIRECTIONS.index(facing)
        facing = DIRECTIONS[(cur_idx + deg//90) % 4]

    return x+dx, y+dy, facing

x, y, facing = 0, 0, 'E'

for (action, val) in moves:
    x, y, facing = move(x, y, facing, action, val)
    # print(f'{action}{val}: {x=}, {y=}, {facing=}')

print(abs(x) + abs(y))
