from collections import defaultdict, Counter, namedtuple

ImageTile = namedtuple('ImageTile', ['n', 'oriented_tile'])

def transpose(l):
    return list(''.join(c) for c in zip(*l))

# from AOC 2017 day 21
def combos(l, flip=True):
    tl = transpose(l)
    yield l # original
    yield [r[::-1] for r in tl] # rotate clockwise 90
    yield [r[::-1] for r in l[::-1]] # rotate clockwise 180
    yield tl[::-1] # rotate clockwise 270
    if flip: # flip vertically
        yield from combos(l[::-1], flip=False)

def left_border(l):   return ''.join(row[0] for row in l)
def right_border(l):  return ''.join(row[-1] for row in l)
def top_border(l):    return l[0]
def bottom_border(l): return l[-1]
def strip_borders(l): return [row[1:-1] for row in l[1:-1]]

tiles = {}
for tile_str in open('20_input.txt').read().split('\n\n'):
    if not tile_str:
        break
    tile_no, *tile = tile_str.splitlines()
    n = int(tile_no.split()[-1].strip(':'))
    tiles[n] = tile

tiles_for_border = defaultdict(set)
for n, tile in tiles.items():
    for c in combos(tile):
        # border and its flipped variant are stored under the same key
        uniq_border = min(c[0], c[0][::-1])
        tiles_for_border[uniq_border].add(n)

def get_tiles_for_border(border):
    border_uniq = min(border, border[::-1])
    return tiles_for_border[border_uniq]

unique_for_tile = Counter(next(iter(v)) for v in tiles_for_border.values() if len(v) == 1)
corners = [t for t, n in unique_for_tile.items() if n == 2] # corners have 2 unique borders
assert len(corners) == 4

IMAGE_TILES = {}
for y in range(12):
    for x in range(12):
        if (x, y) == (0, 0):
            target_tile = corners[0] # pick a random corner to start
            # we're placing first tile to top left corner so those borders must be unique
            target_orientation = next(c for c in combos(tiles[target_tile])
                if len(get_tiles_for_border(left_border(c))) == 1
                and len(get_tiles_for_border(top_border(c))) == 1
            )
        elif x > 0: # check left tile
            left_tile = IMAGE_TILES[x-1, y]
            target_border = right_border(left_tile.oriented_tile)
            target_tile = next(iter(get_tiles_for_border(target_border) - {left_tile.n}))
            # orient target tile so its left_border is target_border
            target_orientation = next(c for c in combos(tiles[target_tile]) if left_border(c) == target_border)
        else: # x == 0, check upper tile
            upper_tile = IMAGE_TILES[x, y-1]
            target_border = bottom_border(upper_tile.oriented_tile)
            target_tile = next(iter(get_tiles_for_border(target_border) - {upper_tile.n}))
            # orient target tile so its top_border is target_border
            target_orientation = next(c for c in combos(tiles[target_tile]) if top_border(c) == target_border)
        IMAGE_TILES[x, y] = ImageTile(target_tile, target_orientation)

IMAGE = []
for y in range(12):
    row_tiles = [strip_borders(IMAGE_TILES[x, y].oriented_tile) for x in range(12)]
    for row_parts in zip(*row_tiles):
        IMAGE.append(''.join(row_parts))

MONSTER = [
    '                  # ',
    '#    ##    ##    ###',
    ' #  #  #  #  #  #   '
]
MONSTER_HEIGHT, MONSTER_WIDTH = len(MONSTER), len(MONSTER[0])
MONSTER_OFFSETS = set((x, y) for y, row in enumerate(MONSTER) for x, c in enumerate(row) if c == '#')

IMAGE_SIZE = len(IMAGE)
monsters_found = 0
for image_combo in combos(IMAGE):
    for y in range(IMAGE_SIZE - MONSTER_HEIGHT):
        for x in range(IMAGE_SIZE - MONSTER_WIDTH):
            if all(image_combo[y+dy][x+dx] == '#' for dx, dy in MONSTER_OFFSETS):
                # assuming monsters don't overlap it's enough to just count them instead of marking each tile
                monsters_found += 1
    if monsters_found > 0:
        print(sum(r.count('#') for r in image_combo) - monsters_found*len(MONSTER_OFFSETS))
        break
