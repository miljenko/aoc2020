from functools import reduce

n = 0
with open('06_input.txt') as f:
    groups = f.read().split('\n\n')
    for group in groups:
        n += len(reduce(set.intersection, (set(i) for i in group.split())))

print(n)
