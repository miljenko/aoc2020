grid = {}
with open('11_input.txt') as f:
    for y, line in enumerate(f):
        for x, c in enumerate(line.strip()):
            grid[x, y] = c
    sizex, sizey = x+1, y+1

def neighbours(x, y):
    for i in (x-1, x, x+1):
        for j in (y-1, y, y+1):
            if 0 <= i < sizex and 0 <= j < sizey and (i, j) != (x, y):
                yield i, j

EMPTY = 'L'
OCCUPIED = '#'

prev_grid = grid.copy()
while True:
    next_grid = prev_grid.copy()
    c = 0
    for i in range(sizex):
        for j in range(sizey):
            if prev_grid[i, j] == EMPTY and not any(prev_grid[s] == OCCUPIED for s in neighbours(i, j)):
                next_grid[i, j] = OCCUPIED
                c += 1
            elif prev_grid[i, j] == OCCUPIED and sum(prev_grid[s] == OCCUPIED for s in neighbours(i, j)) >= 4:
                next_grid[i, j] = EMPTY
                c += 1
    if c == 0:
        break
    else:
        prev_grid = next_grid

print(list(next_grid.values()).count(OCCUPIED))
