l = [16, 1, 0, 18, 12, 14, 19]
spoken_at = {}
n = -1
t = 1
while t <= 2020:
    if t <= len(l):
        n = l[t-1]
    elif len(spoken_at[n]) > 1:
        n = spoken_at[n][-1] - spoken_at[n][-2]
    else:
        n = 0

    if n in spoken_at:
        spoken_at[n] = [spoken_at[n][-1], t]
    else:
        spoken_at[n] = [t]

    t += 1

print(n)
