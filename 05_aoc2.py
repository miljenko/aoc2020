ids = []
with open('05_input.txt') as f:
    for line in f:
        line = line.strip()
        row = int(line[:7].replace('F', '0').replace('B', '1'), 2)
        seat = int(line[7:].replace('L', '0').replace('R', '1'), 2)
        ids.append(row * 8 + seat)

ids.sort()
for i, sid in enumerate(ids):
    if ids[i + 1] == sid + 2:
        print(sid + 1)
        break
