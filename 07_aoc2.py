import re
from collections import defaultdict

contains_re = re.compile(r'(\d+) (\w+ \w+) bag')

d = {}
with open('07_input.txt') as f:
    for line in f:
        l = contains_re.findall(line)
        current = ' '.join(line.split()[:2])
        contains = {t[1]: int(t[0]) for t in l}
        d[current] = contains

goal = 'shiny gold'
bags = defaultdict(int)

def count(bag, m=1):
    for b, n in d[bag].items():
        bags[b] += n*m
        count(b, n*m)

count(goal)
print(sum(bags.values()))
