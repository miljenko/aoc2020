original_code = []
with open('08_input.txt') as f:
    for line in f:
        op, arg = line.split()
        original_code.append((op, int(arg)))

def run(code):
    visited = set()
    pos = 0
    acc = 0
    completed = False

    while pos not in visited:
        if pos >= len(code):
            completed = True
            break
        visited.add(pos)
        op, arg = code[pos]
        if op == 'acc':
            acc += arg
        elif op == 'jmp':
            pos += arg
            continue
        pos += 1

    return completed, acc

for i, (op, arg) in enumerate(original_code):
    if op in ('jmp', 'nop'):
        changed_code = original_code.copy()
        changed_code[i] = ('nop' if op == 'jmp' else 'jmp', arg)
        done, res = run(changed_code)
        if done:
            print(res)
            break
