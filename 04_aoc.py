passports = []
with open('04_input.txt') as f:
    groups = f.read().split('\n\n')
    for group in groups:
        passports.append(dict(f.split(':') for f in group.split()))

mandatory = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}

n = sum(mandatory.issubset(p.keys()) for p in passports)
print(n)
