from functools import reduce
from operator import mul

grid = {}
with open('03_input.txt') as f:
    for y, line in enumerate(f):
        for x, c in enumerate(line.strip()):
            grid[x, y] = c
    sizex, sizey = x+1, y+1

def count(dx, dy):
    x, y = 0, 0
    trees = 0
    while y < sizey:
        if grid[x, y] == '#':
            trees += 1
        x = (x + dx) % sizex
        y += dy

    return trees

slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

print(reduce(mul, (count(*slope) for slope in slopes)))
