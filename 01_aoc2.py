import sys

with open('01_input.txt') as f:
    s = set(int(l) for l in f)

for i in s:
    t = 2020 - i
    for j in s:
        k = t - j
        if k in s:
            print(i*j*k)
            sys.exit()
