from collections import Counter

nums = [0] + sorted(int(n) for n in open('10_input.txt').readlines())
nums.append(nums[-1] + 3)
diffs = [nums[i+1] - nums[i] for i in range(len(nums) - 1)]
c = Counter(diffs)
print(c[1] * c[3])
