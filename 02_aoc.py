import re

pattern = re.compile(r'(\d+)-(\d+) ([a-z]): ([a-z]+)')
valid = 0
with open('02_input.txt') as f:
    for line in f:
        lo, hi, letter, password = pattern.match(line).groups()
        n = password.count(letter)
        if n >= int(lo) and n <= int(hi):
            valid += 1

print(valid)
