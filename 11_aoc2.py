grid = {}
with open('11_input.txt') as f:
    for y, line in enumerate(f):
        for x, c in enumerate(line.strip()):
            grid[x, y] = c
    sizex, sizey = x+1, y+1

DIRECTIONS = [(dx, dy) for dx in (-1, 0, 1) for dy in (-1, 0, 1) if (dx, dy) != (0, 0)]

def visible_seats(g, x, y):
    for (dx, dy) in DIRECTIONS:
        cx, cy = x, y
        while True:
            cx += dx
            cy += dy
            if not (0 <= cx < sizex and 0 <= cy < sizey):
                break
            if g[cx, cy] != '.':
                yield cx, cy
                break

EMPTY = 'L'
OCCUPIED = '#'

prev_grid = grid.copy()
while True:
    next_grid = prev_grid.copy()
    c = 0
    for i in range(sizex):
        for j in range(sizey):
            if prev_grid[i, j] == EMPTY and not any(prev_grid[s] == OCCUPIED for s in visible_seats(prev_grid, i, j)):
                next_grid[i, j] = OCCUPIED
                c += 1
            elif prev_grid[i, j] == OCCUPIED and sum(prev_grid[s] == OCCUPIED for s in visible_seats(prev_grid, i, j)) >= 5:
                next_grid[i, j] = EMPTY
                c += 1
    if c == 0:
        break
    else:
        prev_grid = next_grid

print(list(next_grid.values()).count(OCCUPIED))
