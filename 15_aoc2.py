l = [16, 1, 0, 18, 12, 14, 19]
spoken_at = {}
n = -1
t = 1
while t <= 30000000:
    if t <= len(l):
        n = l[t-1]
        spoken_at[n] = t
    elif n in spoken_at:
        next_n = t-1 - spoken_at[n]
        spoken_at[n] = t-1
        n = next_n
    else:
        spoken_at[n] = t-1
        n = 0

    t += 1

print(n)
