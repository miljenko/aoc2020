import re
from collections import defaultdict

MASKED = {
    # (addr, mask): masked_addr
    ('0', '0'): '0',
    ('0', '1'): '1',
    ('0', 'X'): 'X',
    ('1', '0'): '1',
    ('1', '1'): '1',
    ('1', 'X'): 'X',
}

mem_re = re.compile(r'mem\[(\d+)\] = (\d+)')
memory = defaultdict(int)
mask = ''
with open('14_input.txt') as f:
    for line in f:
        line = line.strip()
        if line.startswith('mask = '):
            mask = line[7:]
        else:
            addr, val = [int(n) for n in mem_re.match(line).groups()]
            bin_addr = f'{addr:0{len(mask)}b}'
            addresses = [''.join(MASKED[a, m] for a, m in zip(bin_addr, mask))]
            while (i := addresses[0].find('X')) != -1:
                addresses = [a[:i] + b + a[i+1:] for a in addresses for b in '01']

            for a in addresses:
                memory[int(a, 2)] = val

print(sum(memory.values()))
