from collections import deque

cups = deque(int(n) for n in '952438716')
MAX_VAL = max(cups)

for m in range(100):
    cups.rotate(-1)
    picked_up = [cups.popleft() for _ in range(3)]
    cups.rotate(1)
    dest = cups[0] - 1 if cups[0] > 1 else MAX_VAL
    while dest in picked_up:
        dest -= 1
        if dest < 1:
            dest = MAX_VAL
    insert_at = cups.index(dest) + 1
    for p in picked_up[::-1]:
        cups.insert(insert_at, p)
    cups.rotate(-1)

cups.rotate(-cups.index(1)) # get 1 to first place
cups.popleft() # remove 1
print(''.join(str(i) for i in cups))
