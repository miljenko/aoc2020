def play_combat(decks):
    seen_decks = set()
    while len(decks[0]) > 0 and len(decks[1]) > 0:
        if decks in seen_decks:
            return decks[0], 0 # player 1 wins
        seen_decks.add(decks)
        c1, deck1 = decks[0][0], decks[0][1:]
        c2, deck2 = decks[1][0], decks[1][1:]

        if len(deck1) >= c1 and len(deck2) >= c2:
            _, round_winner = play_combat((deck1[:c1], deck2[:c2]))
        else:
            round_winner = 0 if c1 > c2 else 1

        if round_winner == 0:
            decks = (deck1 + (c1, c2), deck2)
        else:
            decks = (deck1, deck2 + (c2, c1))

    return decks[round_winner], round_winner

def get_deck(p):
    return tuple(int(card) for card in p.splitlines()[1:])

decks = tuple(get_deck(p) for p in open('22_input.txt').read().split('\n\n'))
winning_deck, _ = play_combat(decks)
print(sum(i*c for i, c in enumerate(winning_deck[::-1], 1)))
