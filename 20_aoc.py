from collections import defaultdict, Counter
from functools import reduce
from operator import mul

def transpose(l):
    return list(''.join(c) for c in zip(*l))

# from AOC 2017 day 21
def combos(l, flip=True):
    tl = transpose(l)
    yield l # original
    yield [r[::-1] for r in tl] # rotate clockwise 90
    yield [r[::-1] for r in l[::-1]] # rotate clockwise 180
    yield tl[::-1] # rotate clockwise 270
    if flip: # flip vertically
        yield from combos(l[::-1], flip=False)

tiles = {}
for tile_str in open('20_input.txt').read().split('\n\n'):
    if not tile_str:
        break
    tile_no, *tile = tile_str.splitlines()
    n = int(tile_no.split()[-1].strip(':'))
    tiles[n] = tile

tiles_for_border = defaultdict(set)
for n, tile in tiles.items():
    for c in combos(tile):
        # border and its flipped variant are stored under the same key
        uniq_border = min(c[0], c[0][::-1])
        tiles_for_border[uniq_border].add(n)

unique_for_tile = Counter(next(iter(v)) for v in tiles_for_border.values() if len(v) == 1)
corners = [t for t, n in unique_for_tile.items() if n == 2] # corners have 2 unique borders
assert len(corners) == 4
print(reduce(mul, corners))
