code = []
with open('08_input.txt') as f:
    for line in f:
        op, arg = line.split()
        code.append((op, int(arg)))

visited = set()
pos = 0
acc = 0

while pos not in visited:
    visited.add(pos)
    op, arg = code[pos]
    if op == 'acc':
        acc += arg
    elif op == 'jmp':
        pos += arg
        continue
    pos += 1

print(acc)
