import re

rules, messages = open('19_input.txt').read().split('\n\n')
rules = dict(rule.split(': ') for rule in rules.split('\n'))
messages = messages.splitlines()

def re_for_rule(rule_no):
    rule = rules[rule_no]
    if re.fullmatch('"."', rule): # single char rule
        return rule[1]

    parts = rule.split('|')
    res = []
    for part in parts:
        nums = part.split()
        res.append(''.join(re_for_rule(num) for num in nums))

    return '(' + '|'.join(res) + ')'

re0 = re_for_rule('0')
print(sum(bool(re.fullmatch(re0, msg)) for msg in messages))
