import sys

nums = [int(n) for n in open('09_input.txt').readlines()]

goal, goal_idx = 0, 0
for i in range(25, len(nums) - 1):
    prev25 = set(nums[i-25: i])
    if not any((nums[i] - j in prev25) for j in prev25):
        goal_idx = i
        goal = nums[i]
        break

for start_idx in range(goal_idx):
    s = nums[start_idx]
    for end_idx in range(start_idx + 1, goal_idx):
        s += nums[end_idx]
        if s == goal:
            l = nums[start_idx: end_idx]
            print(min(l) + max(l))
            sys.exit()
        elif s > goal:
            break
