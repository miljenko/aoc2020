import re

class N(int):
    def __add__(self, other):
        return N(int(self) + int(other))
    def __sub__(self, other): # part 1: - has same precedence as +
        return N(self * other)
    def __and__(self, other): # part 2: & has lower precedence than +
        return N(self * other)

lines = open('18_input.txt').readlines()
for op in ('-', '&'):
    print(sum(eval(re.sub(r'(\d+)', r'N(\1)', line.replace('*', op))) for line in lines))
