import re
from functools import lru_cache

contains_re = re.compile(r'(\d+) (\w+ \w+) bag')

d = {}
with open('07_input.txt') as f:
    for line in f:
        l = contains_re.findall(line)
        current = ' '.join(line.split()[:2])
        contains = {t[1]: t[0] for t in l}
        d[current] = contains

goal = 'shiny gold'

@lru_cache(maxsize=None)
def contains_goal(bag):
    return goal in d[bag] or any(contains_goal(c) for c in d[bag])

print(sum(contains_goal(b) for b in d))
