with open('13_input.txt') as f:
    earliest = int(f.readline())
    buses = [int(n) for n in f.readline().split(',') if n != 'x']

min_t, min_id = float('inf'), -1
for bus in buses:
    t = bus - (earliest % bus)
    if t < min_t:
        min_t = t
        min_id = bus

print(min_t * min_id)
