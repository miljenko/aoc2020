key1, key2 = [int(n) for n in open('25_input.txt').readlines()]

s, i, mod = 1, 1, 20201227
while (s := 7*s % mod) != key1:
    i += 1

print(pow(key2, i, mod))
