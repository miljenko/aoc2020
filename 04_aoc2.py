import re

valid_ecl = {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}
hcl_re = re.compile('#[0-9a-f]{6}')
pid_re = re.compile(r'\d{9}')

def is_valid(field, v):
    if field == 'byr':
        return 1920 <= int(v) <= 2002
    if field == 'iyr':
        return 2010 <= int(v) <= 2020
    if field == 'eyr':
        return 2020 <= int(v) <= 2030
    if field == 'hgt':
        return (v.endswith('cm') and 150 <= int(v[:-2]) <= 193) or (v.endswith('in') and 59 <= int(v[:-2]) <= 76)
    if field == 'hcl':
        return bool(hcl_re.fullmatch(v))
    if field == 'ecl':
        return v in valid_ecl
    if field == 'pid':
        return bool(pid_re.fullmatch(v))
    if field == 'cid':
        return True

passports = []
with open('04_input.txt') as f:
    groups = f.read().split('\n\n')
    for group in groups:
        passports.append(dict(f.split(':') for f in group.split()))

mandatory = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}

n = sum(
    mandatory.issubset(p.keys()) and all(is_valid(k, v) for k, v in p.items())
    for p in passports
)
print(n)
