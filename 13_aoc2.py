from functools import reduce
from math import gcd

def lcm(a, b):
    return (a*b) // gcd(a, b)

def lcm_list(l):
    return reduce(lcm, l)

with open('13_input.txt') as f:
    f.readline()
    buses = [(int(n), i) for i, n in enumerate(f.readline().split(',')) if n != 'x']

t = 0
step = 1
synced_so_far = 0
while True:
    synced = [bus_id for bus_id, bus_idx in buses if (t + bus_idx) % bus_id == 0]
    if len(synced) == len(buses):
        print(t)
        break
    if len(synced) > synced_so_far: # new bus in sync
        step = lcm_list(synced)
        synced_so_far = len(synced)
    t += step

# same as AOC2016 day 15, but t is too big here to increase it by 1
# t = 0
# while any((t + bus_idx) % bus_id != 0 for bus_id, bus_idx in buses.items()):
#     t += 1
# print(t)

# we could use Chinese Remainder Theorem
# from sympy.ntheory.modular import crt
# print(crt([b[0] for b in buses], [-b[1] for b in buses])[0])
