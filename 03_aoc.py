grid = {}
with open('03_input.txt') as f:
    for y, line in enumerate(f):
        for x, c in enumerate(line.strip()):
            grid[x, y] = c
    sizex, sizey = x+1, y+1

dx, dy = 3, 1
x, y = 0, 0
trees = 0

while y < sizey:
    if grid[x, y] == '#':
        trees += 1
    x = (x + dx) % sizex
    y += dy

print(trees)
