nums = [int(n) for n in open('09_input.txt').readlines()]

for i in range(25, len(nums) - 1):
    prev25 = set(nums[i-25: i])
    if not any((nums[i] - j in prev25) for j in prev25):
        print(nums[i])
        break
