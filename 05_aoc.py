max_id = -1
with open('05_input.txt') as f:
    for line in f:
        line = line.strip()
        row = int(line[:7].replace('F', '0').replace('B', '1'), 2)
        seat = int(line[7:].replace('L', '0').replace('R', '1'), 2)
        sid = row * 8 + seat
        if sid > max_id:
            max_id = sid

print(max_id)
