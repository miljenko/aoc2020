import re

range_re = re.compile(r'(\d+)-(\d+)')
ranges = {}
ticket = []
nearby_tickets = []

with open('16_input.txt') as f:
    for line in f:
        if not line.strip():
            break
        field = line.split(':')[0]
        v = set()
        for r in range_re.findall(line):
            lo, hi = r
            v.update(range(int(lo), int(hi)+1))
        ranges[field] = v

    f.readline() # your ticket:
    ticket = [int(n) for n in f.readline().split(',')]
    f.readline() # \n
    f.readline() # nearby tickets:
    nearby_tickets = [[int(n) for n in l.split(',')] for l in f]

e = 0
for nt in nearby_tickets:
    for nt_n in nt:
        if not any(nt_n in ranges[f] for f in ranges):
            e += nt_n
            break

print(e)
