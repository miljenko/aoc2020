import string
from collections import deque
from functools import reduce
from operator import mul

expressions = []
with open('18_input.txt') as f:
    for line in f:
        expressions.append([int(c) if c in string.digits else c for c in line if not c.isspace()])

def calc(l):
    # process additions first
    while '+' in l:
        add_i = l.index('+')
        l[add_i-1:add_i+2] = [l[add_i-1] + l[add_i+1]]

    # multiply the rest of the numbers (skip the operators)
    return reduce(mul, l[::2])

total = 0
for expr in expressions:
    stack = deque()
    for c in expr:
        if c != ')':
            stack.append(c)
        else:
            in_parens = []
            while (s := stack.pop()) != '(':
                in_parens.append(s)
            in_parens.reverse()
            stack.append(calc(in_parens))

    total += calc(list(stack))

print(total)
