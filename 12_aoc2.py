moves = []
with open('12_input.txt') as f:
    for line in f:
        moves.append((line[0], int(line[1:])))

DIRECTIONS = 'NESW' # clockwise order

def step(action, val):
    if action == 'N': return 0, val
    if action == 'E': return val, 0
    if action == 'S': return 0, -val
    if action == 'W': return -val, 0

def move(x, y, wx, wy, action, val):
    if action in DIRECTIONS:
        dwx, dwy = step(action, val)
        return x, y, wx+dwx, wy+dwy
    if action == 'F':
        return x + wx*val, y + wy*val, wx, wy
    if action in 'RL':
        for _ in range(val // 90):
            wx, wy = (wy, -wx) if action == 'R' else (-wy, wx)
        return x, y, wx, wy

x, y = 0, 0
wx, wy = 10, 1

for (action, val) in moves:
    x, y, wx, wy = move(x, y, wx, wy, action, val)
    # print(f'{action}{val}: {x=}, {y=}, {wx=}, {wy=}')

print(abs(x) + abs(y))
