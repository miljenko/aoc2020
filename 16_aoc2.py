import re
from functools import reduce
from operator import mul

range_re = re.compile(r'(\d+)-(\d+)')
ranges = {}
ticket = []
nearby_tickets = []

with open('16_input.txt') as f:
    for line in f:
        if not line.strip():
            break
        field = line.split(':')[0]
        v = set()
        for r in range_re.findall(line):
            lo, hi = r
            v.update(range(int(lo), int(hi)+1))
        ranges[field] = v

    f.readline() # your ticket:
    ticket = [int(n) for n in f.readline().split(',')]
    f.readline() # \n
    f.readline() # nearby tickets:
    nearby_tickets = [[int(n) for n in l.split(',')] for l in f]

valid_tickets = []
for nt in nearby_tickets:
    for nt_n in nt:
        if not any(nt_n in ranges[f] for f in ranges):
            break
    else:
        valid_tickets.append(nt)

possible = {f: set() for f in ranges}
N_FIELDS = len(ranges)
for f in ranges:
    for i in range(N_FIELDS):
        vals = {vt[i] for vt in valid_tickets}
        if vals.issubset(ranges[f]):
            possible[f].add(i)

finalized = {}
while len(finalized) < N_FIELDS:
    for f, possible_values in possible.items():
        if len(possible_values) == 1:
            finalized[f] = next(iter(possible_values))
    for f in possible:
        if f not in finalized:
            possible[f] -= set(finalized.values())

target_indices = {v for f, v in finalized.items() if f.startswith('departure')}
print(reduce(mul, (v for i, v in enumerate(ticket) if i in target_indices)))
