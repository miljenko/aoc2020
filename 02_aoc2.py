import re

pattern = re.compile(r'(\d+)-(\d+) ([a-z]): ([a-z]+)')
valid = 0
with open('02_input.txt') as f:
    for line in f:
        lo, hi, letter, password = pattern.match(line).groups()
        lo_contains = password[int(lo)-1] == letter
        hi_contains = password[int(hi)-1] == letter
        if lo_contains ^ hi_contains:
            valid += 1

print(valid)
