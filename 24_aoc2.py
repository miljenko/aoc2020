from collections import defaultdict
import re

dirs = {
    'nw': (0, 1, -1),
    'ne': (1, 0, -1),
    'e':  (1, -1, 0),
    'se': (0, -1, 1),
    'sw': (-1, 0, 1),
    'w':  (-1, 1, 0),
}

def neighbours(x, y, z):
    for dx, dy, dz in dirs.values():
        yield x+dx, y+dy, z+dz

def next_value(pos, prev_grid, val):
    black_neighbours = sum(prev_grid[np] for np in neighbours(*pos) if np in prev_grid)
    if ((val == BLACK and (black_neighbours == 0 or black_neighbours > 2))
        or (val == WHITE and black_neighbours == 2)):
        return not val
    return val

tile_re = re.compile('e|se|sw|w|nw|ne')
tiles = [tile_re.findall(line) for line in open('24_input.txt')]

WHITE, BLACK = False, True
grid = defaultdict(lambda: WHITE)
start = 0, 0, 0
for tile in tiles:
    x, y, z = start
    for d in tile:
        dx, dy, dz = dirs[d]
        x, y, z = x+dx, y+dy, z+dz
    grid[x, y, z] = not grid[x, y, z]

prev_grid = grid.copy()
for day in range(100):
    next_grid = prev_grid.copy()
    for pos, v in prev_grid.items():
        next_grid[pos] = next_value(pos, prev_grid, v)
        # process neighbours too to grow the grid
        for neighbour_pos in neighbours(*pos):
            if neighbour_pos not in next_grid:
                next_grid[neighbour_pos] = next_value(neighbour_pos, prev_grid, WHITE)
    prev_grid = next_grid

print(sum(next_grid.values()))
