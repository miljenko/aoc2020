import re

rules, messages = open('19_input.txt').read().split('\n\n')
rules = dict(rule.split(': ') for rule in rules.split('\n'))
messages = messages.splitlines()

def re_for_rule(rule_no):
    if rule_no == '8': # 8: 42 | 42 8 -> 42 42 42 ...
        return re_for_rule('42') + '+'
    if rule_no == '11': # 11: 42 31 | 42 11 31 -> 42 42 ... 31 31 - same amount of times
        re42 = re_for_rule('42')
        re31 = re_for_rule('31')
        # all possible regexes up to length 20 (5 is the min that gives correct result for this input)
        return '(' + '|'.join(f'{re42}{{{n}}}{re31}{{{n}}}' for n in range(1, 20)) + ')'

    rule = rules[rule_no]
    if re.fullmatch('"."', rule): # single char rule
        return rule[1]

    parts = rule.split('|')
    res = []
    for part in parts:
        nums = part.split()
        res.append(''.join(re_for_rule(num) for num in nums))

    return '(' + '|'.join(res) + ')'

re0 = re_for_rule('0')
print(sum(bool(re.fullmatch(re0, msg)) for msg in messages))
