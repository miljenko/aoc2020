from collections import defaultdict
import re

dirs = {
    'nw': (0, 1, -1),
    'ne': (1, 0, -1),
    'e':  (1, -1, 0),
    'se': (0, -1, 1),
    'sw': (-1, 0, 1),
    'w':  (-1, 1, 0),
}

tile_re = re.compile('e|se|sw|w|nw|ne')
tiles = [tile_re.findall(line) for line in open('24_input.txt')]

WHITE, BLACK = False, True
grid = defaultdict(lambda: WHITE)
start = 0, 0, 0
for tile in tiles:
    x, y, z = start
    for d in tile:
        dx, dy, dz = dirs[d]
        x, y, z = x+dx, y+dy, z+dz
    grid[x, y, z] = not grid[x, y, z]

print(sum(grid.values()))
