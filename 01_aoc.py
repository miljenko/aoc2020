with open('01_input.txt') as f:
    s = set(int(l) for l in f)

for i in s:
    j = 2020 - i
    if j in s:
        print(i*j)
        break
