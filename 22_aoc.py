from collections import deque

decks = []
for player in open('22_input.txt').read().split('\n\n'):
    decks.append(deque(int(card) for card in player.splitlines()[1:]))

while len(decks[0]) > 0 and len(decks[1]) > 0:
    c1 = decks[0].popleft()
    c2 = decks[1].popleft()
    if c1 > c2:
        decks[0].extend([c1, c2])
    else:
        decks[1].extend([c2, c1])

winning_deck = next(d for d in decks if len(d) > 0)
winning_deck.reverse()
print(sum(i*c for i, c in enumerate(winning_deck, 1)))
