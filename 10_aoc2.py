from functools import lru_cache

nums = [0] + sorted(int(n) for n in open('10_input.txt').readlines())
goal = nums[-1] + 3
nums.append(goal)

d = {n: [i for i in nums if n-3 <= i < n] for n in nums}

@lru_cache(maxsize=None)
def cnt(cur):
    if cur == 0:
        return 1
    return sum(cnt(nxt) for nxt in d[cur])

print(cnt(goal))
