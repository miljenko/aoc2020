from collections import Counter

ALL_INGREDIENTS = set()
ALL_ALLERGENS = set()
ALLERGEN_IN = {}
INGR_APPEARANCES = Counter()

with open('21_input.txt') as f:
    for line in f:
        ins, als = line.split('(contains')
        ingrs = ins.split()
        alrgs = als.strip().rstrip(')').split(', ')
        INGR_APPEARANCES.update(ingrs)
        ALL_INGREDIENTS.update(ingrs)
        ALL_ALLERGENS.update(alrgs)
        for alrg in alrgs:
            if alrg in ALLERGEN_IN:
                ALLERGEN_IN[alrg] &= set(ingrs)
            else:
                ALLERGEN_IN[alrg] = set(ingrs)

while True:
    FOUND_ALLERGENS = set(a for a, i in ALLERGEN_IN.items() if len(i) == 1)
    DANGEROUS_INGREDIENTS = set(next(iter(ALLERGEN_IN[a])) for a in FOUND_ALLERGENS)

    if FOUND_ALLERGENS == ALL_ALLERGENS:
        break

    for alrg in ALLERGEN_IN:
        if alrg in FOUND_ALLERGENS:
            continue
        ALLERGEN_IN[alrg] -= DANGEROUS_INGREDIENTS

SAFE_INGREDIENTS = ALL_INGREDIENTS - DANGEROUS_INGREDIENTS

print(sum(INGR_APPEARANCES[i] for i in SAFE_INGREDIENTS))
