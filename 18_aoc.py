import string
from collections import deque

expressions = []
with open('18_input.txt') as f:
    for line in f:
        expressions.append([int(c) if c in string.digits else c for c in line if not c.isspace()])

def calc(l):
    r = l[0]
    i = 1
    while i < len(l):
        op, n = l[i], l[i+1]
        if op == '*':
            r *= n
        else:
            r += n
        i += 2

    return r

total = 0
for expr in expressions:
    stack = deque()
    for c in expr:
        if c != ')':
            stack.append(c)
        else:
            in_parens = []
            while (s := stack.pop()) != '(':
                in_parens.append(s)
            in_parens.reverse()
            stack.append(calc(in_parens))

    total += calc(stack)

print(total)
