import re
from collections import defaultdict

mem_re = re.compile(r'mem\[(\d+)\] = (\d+)')
memory = defaultdict(int)
mask = ''
with open('14_input.txt') as f:
    for line in f:
        line = line.strip()
        if line.startswith('mask = '):
            mask = line[7:]
        else:
            addr, val = [int(n) for n in mem_re.match(line).groups()]
            bin_val = f'{val:0{len(mask)}b}'
            masked_val = int(''.join(v if m == 'X' else m for v, m in zip(bin_val, mask)), 2)
            memory[addr] = masked_val

print(sum(memory.values()))
